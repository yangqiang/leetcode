/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年05月09日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    bool isIsomorphic(string s, string t) {
        int size = s.size();

        vector<int> tmps(128, -1);
        vector<int> tmpt(128, -2);
        for(int i = 0; i < size; i++){
            if(tmps[s[i]] == -1 && tmpt[t[i]] == -2){
                tmps[s[i]] = i;
                tmpt[t[i]] = i;
            }else if(tmps[s[i]] != tmpt[t[i]]){
                return false;
            }
        }

        return true;
    }
};

