/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年09月09日
*   描    述：
*   Copyright (C) 2018 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    /**
     * @param root: The root of binary tree.
     * @return: True if the binary tree is BST, or false
     */
    bool isValidBST(TreeNode * root) {
        // write your code here
        if( !root )
            return true;
        return isbst(root, NULL, NULL);
    }

    bool isbst(TreeNode *root, TreeNode *minnode, TreeNode *maxnode){
       if( !root )
           return true;
       if( (minnode && root->val <= minnode->val) || (maxnode && root->val >= maxnode->val ) )
           return false;

       bool left = isbst(root->left, minnode, root);
       if( !left )
           return false;

       bool right = isbst(root->right, root, maxnode);

       return right;
    }

};
