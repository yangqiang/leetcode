/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年03月08日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */


class Solution {
public:
    vector<ListNode*> splitListToParts(ListNode* root, int k) {
        vector<ListNode*> res(k, NULL);
        if(!root){
            return res;
        }

        ListNode* node = root, *pre = root;
        int len = 0;
        while(node){
            len++;
            node = node->next;
        }

        int n = len/k, remaind = len%k;
        node = root;
        for(int i = 0; i < k; i++, remaind--){
            res[i] = node;
            for(int j = 0; j < n + (remaind>0?1:0); j++){
                pre = node;
                node = node->next;
            }

            pre->next = NULL;
        }

        return res;
    }
};