class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        vector<int> ret(2, -1);
        if( nums.size() == 0 || (nums.size() == 1 && nums[0] != target ))
            return ret;
        
        int left = 0, right = nums.size() - 1;
        int mid = 0;
        while( left < right ){
            mid = left + (right - left)/2;
            if( nums[mid] < target )
                left = mid + 1;
            else
                right  = mid;
        }

        if( nums[left] != target )
            return ret;
        ret[0] = left;

        right = nums.size() - 1;
        while( left < right ){
            mid = left + (right - left)/2 + 1;
            if( nums[mid] > target  )
                right = mid - 1;
            else
                left = mid;
        }
        ret[1] = right;
        
        return ret;
            
    }
};
