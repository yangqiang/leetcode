#!/usr/bin/env python3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年05月06日
描    述：
Copyright (C) 2019 All rights reserved.
'''

class Solution:
    def eraseOverlapIntervals(self, intervals: List[List[int]]) -> int:
        size = len(intervals)
        if size == 0:
            return 0
        
        intervals = sorted(intervals, key=lambda x:x[1])

        end = intervals[0][1]
        count = 1

        for i in range(1,size):
            if intervals[i][0] >= end:
                end = intervals[i][1]
                count += 1

        return size - count
        
