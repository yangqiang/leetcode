#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年06月20日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''


class Solution(object):
    def validIPAddress(self, IP):
        """
        :type IP: str
        :rtype: str
        """
        if '.' in IP:
            return self.check_ipv4(IP)

        if ':' in IP:
            return self.check_ipv6(IP)

        return "Neither"

    def check_ipv4(self, IPV4):
        ipv4s = IPV4.split('.')
        if len(ipv4s) != 4:
            return "Neither"
        for ipv4 in ipv4s:
            if len(ipv4) == 0 or len(ipv4) > 3:
                return "Neither"

            if (ipv4[0] == '0' and len(ipv4) > 1) or not ipv4.isdigit() or int(ipv4) > 255:
                return "Neither"

        return "IPv4"

    def check_ipv6(self, IPV6):
        ipv6s = IPV6.split(':')
        hexdigits = '0123456789abcdefABCDEF'
        if len(ipv6s) != 8:
            return "Neither"

        for ipv6 in ipv6s:
            if len(ipv6) == 0 or len(ipv6) > 4:
                return "Neither"

            for x in ipv6:
                if x not in hexdigits:
                    return "Neither"

        return "IPv6"


