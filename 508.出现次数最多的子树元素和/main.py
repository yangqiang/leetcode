#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年09月14日
版    本：v1.0.0
描    述：
Copyright (C) 2019 All rights reserved.
'''


# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def __init__(self):
        self.res = list()
        self.datamap = dict()

    def fineTreeSum(self, root):
        leftval = 0
        rightval = 0
        if root.left != None:
            leftval = self.fineTreeSum(root.left)
        if root.right != None:
            rightval = self.fineTreeSum(root.right)

        sum = leftval + rightval + root.val
        oldval = self.datamap.get(sum, 0)
        self.datamap[sum] = oldval + 1

        return sum

    def findFrequentTreeSum(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """

        if root == None:
            return self.res

        self.fineTreeSum(root)

        maxtime = 0
        for key, val in self.datamap.items():
            if val > maxtime:
                self.res = []
                self.res.append(key)
                maxtime = val
            elif val == maxtime:
                self.res.append(key)
            else:
                continue

        return self.res
