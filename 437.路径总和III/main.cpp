/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年04月27日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int result = 0;
    int pathSum(TreeNode* root, int sum) {
        if( root == NULL )
            return 0;

        return pathCurSum(root, sum) + pathSum(root->left, sum) + pathSum(root->right, sum);
    }

private:
    int pathCurSum(TreeNode* root, int sum){
        if(root == NULL)
            return 0;

        return (root->val == sum ? 1:0) + parhCurSum(root->left, sum-root->val) + pathCurSum(root->right, sum-root->val);
    }
};

