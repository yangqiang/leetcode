#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int removeElement(vector<int>& nums, int val) {
        int len = nums.size();
        if( len == 0 )
            return 0;

        int i = 0, j = 0;
        for( j; j < len; j++ ){
            if( nums[j] != val ){
                nums[i++] = nums[j];
            }
        }

        return i;
    }

};
