/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年11月17日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    double findMaxAverage(vector<int>& nums, int k) {
        int size = nums.size();
        double sum = 0, average = 0;
        for(int i = 0; i < k; i++){
            sum += nums[i];
        }

        average = sum/k;

        for(int i = k; i < size; i++){
            sum = sum - nums[i-k] + nums[i];
            average = max(average, sum/k);
        }

        return average;
    }
};
