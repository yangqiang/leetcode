/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年12月03日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* constructMaximumBinaryTree(vector<int>& nums) {
        int size = nums.size();
        TreeNode* root = NULL;
        return constructBinaryTree(nums, 0, size - 1);
    }

    TreeNode* constructBinaryTree(vector<int>& nums, int start, int end){
        if(start > end){
            return NULL;
        }

        int index = getMax(nums, start, end);
        TreeNode *root = new TreeNode(nums[index]);
        root->left = constructBinaryTree(nums, start, index-1);
        root->right = constructBinaryTree(nums, index+1, end);

        return root;
    }

    int getMax(vector<int>& nums, int start, int end){ 
        int index = start; 
        for(int i = start;i <= end;i++){ 
            if(nums[index] < nums[i]){ 
                index = i;
            } 
        } 
        
        return index;
    }
};