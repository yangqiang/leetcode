/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年05月23日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <math.h>
using namespace std;

class Solution {
public:
    int bulbSwitch(int n) {
        return sqrt(n); 
    }
};
