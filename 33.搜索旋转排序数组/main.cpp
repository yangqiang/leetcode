class Solution {
public:
    int search(vector<int>& nums, int target) {
        int left = 0, right = nums.size() - 1;
        int mid = 0;
        while( left < right ){
            mid = left + (right - left)/2;
           if( nums[mid] > nums[right] )
               left = mid + 1;
           else
               right = mid;
        }

        if( nums[mid] == target )
            return mid;
        
        right = nums.size() - 1;
        left = 0;
        if( target > nums[right] )
            right = mid - 1;
        else
            left = mid + 1;

        while( left <= right ){
            mid = left + (right - left)/2;
            if( nums[mid] == target)
                return mid;
            if( nums[mid] > target )
                right = mid -1;
            else
                left = mid + 1; 
        }

        return -1;
    }
};

class Solution {
public:
    int search(vector<int>& nums, int target) {
        int n = (int)nums.size();
        if (!n) return -1;
        if (n == 1) return nums[0] == target ? 0 : -1;
        int l = 0, r = n - 1;
        while (l <= r) {
            int mid = (l + r) / 2;
            if (nums[mid] == target) return mid;
            if (nums[0] <= nums[mid]) {
                if (nums[0] <= target && target < nums[mid]) {
                    r = mid - 1;
                } else {
                    l = mid + 1;
                }
            } else {
                if (nums[mid] < target && target <= nums[n - 1]) {
                    l = mid + 1;
                } else {
                    r = mid - 1;
                }
            }
        }
        return -1;
    }
};
