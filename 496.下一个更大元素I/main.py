#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年09月10日
描    述：
Copyright (C) 2019 All rights reserved.
'''


class Solution(object):
    def nextGreaterElement(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: List[int]
        """
        length = len(nums2)
        hashmap = {}
        ans = list()
        s = list()

        for i in range(length):
            num = nums2[i]
            hashmap[num] = -1
            if len(s) > 0:
                for j in range(len(s)-1, -1, -1):
                    if num > s[j]:
                        hashmap[s[j]] = num
                        s.remove(s[j])
            s.append(num)

        length = len(nums1)
        for i in range(length):
            ans.append(hashmap.get(nums1[i]))

        return ans




            

