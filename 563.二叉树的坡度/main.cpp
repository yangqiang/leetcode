/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年01月04日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <cmath>
using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int res = 0;
public:
    int findTilt(TreeNode* root) {
        getTilt(root);
        return res;
    }

    int getTilt(TreeNode *root){
        if(root == NULL){
            return 0;
        }

        int left = getTilt(root->left);
        int right = getTilt(root->right);

        res += abs(left-right);

        return left+right+root->val;
    }
};