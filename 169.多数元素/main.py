#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年06月15日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''


class Solution(object):
    def majorityElement(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        num = nums[0]
        count = 1
        for i in range(1, len(nums)):
            if count == 0:
                num = nums[i]
                count = 1
                continue

            if num == nums[i]:
                count += 1
            else:
                count -= 1

        return num
