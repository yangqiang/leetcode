#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年07月08日
描    述：
'''


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution:
    def searchBinary(self, root, path, res):
        if not root.left and not root.right:
            res.append(path+str(root.val))
            return
        if root.left:
            self.searchBinary(root.left, path+str(root.val)+"->", res)
        if root.right:
            self.searchBinary(root.right, path+str(root.val)+"->", res)

    def binaryTreePaths(self, root):
        res = list()
        if root is None:
            return res

        self.searchBinary(root, "", res)
        return res
