/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年07月09日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <vector>
#include <string>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<string> binaryTreePaths(TreeNode* root) {
        vector<string> res;
        if( !root ){
            return res;
        }

        searchBinary(root, "", res);
        return res;
    }

private:
    void searchBinary(TreeNode *root, string path, vector<string> &res){
        if( !root->left && !root->right){
            res.push_back(path+to_string(root->val));
            return;
        }

        if( root->left){
            searchBinary(root->left, path+to_string(root->val)+"->", res);
        }
        if( root->right){
            searchBinary(root->right, path+to_string(root->val)+"->", res);
        }
    }
};