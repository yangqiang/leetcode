/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年05月18日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

class Solution {
public:
    bool escapeGhosts(vector<vector<int>>& ghosts, vector<int>& target) {
        vector<int>source(2, 0);
        int size = ghosts.size();
        int dst = taxi(source, target);
        for(int i = 0; i < size; i++){
            if(taxi(ghosts[i], target) <= dst )
                return false;
        }

        return true;
    }

    int taxi(vector<int> &q, vector<int> &p){
        return abs(q[0] - p[0]) + abs(q[1] - p[1]);
    }
};