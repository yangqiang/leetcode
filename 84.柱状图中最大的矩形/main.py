#!/usr/bin/env python3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年02月28日
描    述：
Copyright (C) 2019 All rights reserved.
'''

class Solution(object):
    def largestRectangleArea(self, heights):
        """
        :type heights: List[int]
        :rtype: int
        """
        length = len(heights)
        if length == 0:
            return 0

        right = 1
        maxarea = heights[0]
        for i in range(1, length):
            if heights[i] < heights[0]:
                break
	    else:
                left = i

        maxarea = max(maxarea, (left + 1) * heights[0])

        left = length - 1
        for i in range(length - 2, -1, -1):
            if heights[i] < heights[length - 1]:
                break
            else:
                left = i
        maxarea = max(maxarea, (length - left) * heights[length - 1])

        for i in range(1, length - 1):
            left = i
            right = i
            k = i
            for j in range( i + 1, length):
                if heights[j] < heights[i]:
                    break
            right = j - 1
            for k in range(i - 1, -1, -1):
                if heights[k] < heights[i]:
                    break
            left = k + 1

            maxarea = max(maxarea, (right - left + 1) * heights[i])

        return maxarea
