/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年06月19日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    vector<int> countBits(int num) {
        vector<int>res(num+1, 0);
        for(int i = 1; i <= num; i++){
            res[i] = res[i & (i-1)] + 1;
        }

        return res;
    }
};
