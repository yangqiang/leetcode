class Solution {
public:
    int firstMissingPositive(vector<int>& nums) {
        int len = nums.size();
        int ret = 1;
        for(int i = 0; i < len; i++){
            while(nums[i] > 0 && nums[i] <= len && nums[i] < i + 1 && nums[i] != nums[nums[i] - 1])
                swap(nums[i], nums[nums[i] - 1]);
        }
        
        for(int j = 0; j < len; j++){
            if( nums[j] != j + 1)
                return j + 1;
        }
        return len + 1;
    }
};
