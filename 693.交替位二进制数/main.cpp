/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年12月22日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>

using namespace std;

class Solution {
public:
    bool hasAlternatingBits(int n) {
        while(n != 0){
            int flag = n%2;
            n = n>>1;
            if( (flag ^ n%2)  == 0 ){
                return false;
            }
        }

        return true;
    }
};