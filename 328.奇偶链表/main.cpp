/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年01月06日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

class Solution {
public:
    ListNode* oddEvenList(ListNode* head) {
        if( !head || !head->next )
            return head;

        ListNode* sig = head, *evenhead = head->next, *even = head->next;

        while( even && even->next ){
            sig->next = sig->next->next;
            even->next = even->next->next;
            sig = sig->next;
            even = even->next;
        }

        sig->next = evenhead;

        return head;
    }
};
