/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年02月17日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;

class Solution {
public:
    bool isValidSerialization(string preorder) {
        int len = preorder.length();
        int num = 1;
        for(int i = 0; i < len; i++){
            if( num == 0 )
                return false;

            if(preorder[i] == ',')
                continue;

            if(preorder[i] == '#'){
                num--;
                continue;
            }
            
            while(i < len && preorder[i] != ',' && preorder[i] != '#' )
                i++;
            
            i--;
            num++;
        }

        return num == 0;
    }
};
