#!/usr/bin/env python3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年02月17日
描    述：
Copyright (C) 2019 All rights reserved.
'''

class Solution:
    def isValidSerialization(self, preorder: 'str') -> 'bool':
        preorder = preorder.split(',')
        num = 1
        for i in preorder:
            if num == 0:
                return False

            if i == '#':
                num = num - 1
            else:
                num = num + 1
        return num == 0
