/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年10月21日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int findMaxConsecutiveOnes(vector<int>& nums) {
        int size = nums.size();
        int res = 0, count = 0;
        for(int i = 0; i < size; i++){
            if(nums[i] == 1){
                count++;
                res = max(res, count);
            }else{
                count = 0;
            }
        }

        return res;
    }
};