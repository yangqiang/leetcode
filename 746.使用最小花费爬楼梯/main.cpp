/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年03月12日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int minCostClimbingStairs(vector<int>& cost) {
        int size =  cost.size();
        if(size == 0){
            return 0;
        }

        if(size == 1){
            return cost[0];
        }

        int first = cost[size - 1];
        int second = cost[size - 2];
        int tmp;
        for(int i = size - 3; i >= 0; i--){
            tmp = cost[i] + min(first, second);
            first = second;
            second = tmp;
        }

        return min(first, second);
    }
};