/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年02月15日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;

class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
  		int m = matrix.size();
        if( m == 0 )
            return false;

        int n = matrix[0].size();
        if( n == 0 )
            return false;

        int i = 0, j = n - 1;
        while( i < m && j >= 0 ){
            if( target == matrix[i][j] )
                return true;

            if( target < matrix[i][j] )
                j--;
            else
                i++;
        }

        return false;
    }
};

