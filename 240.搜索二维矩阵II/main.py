#!/usr/bin/env pytho3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年02月15日
描    述：
Copyright (C) 2019 All rights reserved.
'''

class Solution:
    def searchMatrix(self, matrix, target):
        """
        :type matrix: List[List[int]]
        :type target: int
        :rtype: bool
        """
        m = len(matrix)
        if m == 0:
            return False

        n = len(matrix[0])
        if n == 0:
            return False

        i = 0; j = n - 1
        while i < m and j >= 0:
            if target == matrix[i][j]:
                return True
            if target < matrix[i][j]:
                j = j - 1
            else:
                i = i + 1

        return False
