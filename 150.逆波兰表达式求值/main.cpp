/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年01月05日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <stack>
#include <vector>
using namespace std;

class Solution {
public:
    int evalRPN(vector<string>& tokens) {
        stack<int> st;
        int len = tokens.size();
        int j = 0, k = 0;
        for(int i = 0; i < len; i++){
            if( tokens[i] == "+" || tokens[i] =="-" || tokens[i] == "*" || tokens[i] == "/" ){
                k = st.top();
                st.pop();
                j = st.top();
                st.pop();
                if( tokens[i] == "+" )
                    st.push(j+k);
                else if( tokens[i] == "-" )
                    st.push(j - k);
                else if( tokens[i] == "*" )
                    st.push(j*k);
                else
                    st.push(j/k);
            }else
                st.push(stoi(tokens[i]));
        }

        return st.top();
    }
};
