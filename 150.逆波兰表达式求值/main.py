#!/usr/bin/env pytho3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年01月05日
描    述：
Copyright (C) 2019 All rights reserved.
'''

class Solution:
    def evalRPN(self, tokens):
        """
        :type tokens: List[str]
        :rtype: int
        """
        res = 0;
        st = []
        for i in tokens:
            if i not in ["+", "-", "*", "/"]:
                st.append(i)
                continue
            else:
                j, k = st.pop(), st.pop()
                if i == "+":
                    st.append( j + k )
                elif i == "-":
                    st.append( j - k )
                elif i == "*":
                    st.append( j * k )
                else:
                    st.append( j / k )

        return st.pop()
