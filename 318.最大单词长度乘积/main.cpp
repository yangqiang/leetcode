/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年11月04日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution {
public:
    int maxProduct(vector<string>& words) {
        int size = words.size();
        if(size < 2){
            return 0;
        }    

        vector<int> str;
        vector<int> bi;
        for(int i = 0; i < size; i++){
            int str_size = words[i].size();
            int d = 0, tmp = 0, j = 0;
            for(j = 0; j < str_size; j++){
                tmp = 1 << int(words[i][j] - 'a');
                d = d | tmp;
            }
            
            str.push_back(str_size);
            bi.push_back(d);
        }

        int res = 0;
        for(int i = 0; i < size-1; i++){
            for(int j = i+1; j < size; j++){
                if((bi[i] & bi[j]) == 0){
                    res = max(res, str[i] * str[j]);
                }
            }
        }

        return res;
    }
};
