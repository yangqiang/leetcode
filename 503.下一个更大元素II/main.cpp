/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年09月10日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
#include <stack>

using namespace std;


class Solution {
public:
    vector<int> nextGreaterElements(vector<int>& nums) {
        int len = nums.size();
        stack<int> s;
        vector<int> res(len, -1);

        for(int i = len - 1; i >= 0; i--){
            while(!s.empty() && s.top() <= nums[i]){
                s.pop();
            }

            if(!s.empty()){
                res[i] = s.top();
            }else{
                for(int j = 0; j < i; j++){
                    if(nums[j] > nums[i]){
                        res[i] = nums[j];
                        break;
                    }
                }
            }

            s.push(nums[i]);
        }

        return res;
    }
};
