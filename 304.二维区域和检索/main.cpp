/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年08月16日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <vector>
using namespace std;


class NumMatrix {
private:
    int row = 0, col = 0;
    vector< vector<int> > dp;

public:
    NumMatrix(vector<vector<int>>& matrix) {
        row = matrix.size();
        if( row == 0 )
            return;

        col = matrix[0].size();
        dp =  vector<vector<int>>(row+1, vector<int>(col+1, 0));
        
        for(int i = row - 1; i >= 0; i--){
            for(int j = col - 1; j >= 0; j--){
                dp[i][j] = dp[i+1][j] + dp[i][j+1] + matrix[i][j] - dp[i+1][j+1];
            }
        }
        return;

    }
    
    int sumRegion(int row1, int col1, int row2, int col2) {
        if( row1 < 0 || row2 >= row )
            return 0;

        if(col1 < 0 || col2 >= col)
            return 0;

        return dp[row1][col1] - dp[row2+1][col1] - dp[row1][col2+1] + dp[row2+1][col2+1];
    }
};

/**
 * Your NumMatrix object will be instantiated and called as such:
 * NumMatrix* obj = new NumMatrix(matrix);
 * int param_1 = obj->sumRegion(row1,col1,row2,col2);
 */
