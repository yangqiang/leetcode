/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年08月11日
*   描    述：
*   
================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        if( k <= 0 ){
            return;
        }

        k %= nums.size();
        reverse(nums, 0, nums.size()-1);
        reverse(nums, 0, k - 1);
        reverse(nums, k, nums.size() - 1);
    }

    void reverse(vector<int> &nums, int start, int end){
        while( start < end ){
            swap(nums[start++], nums[end--]);
        }
    }
};