#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年09月14日
版    本：v1.0.0
描    述：
Copyright (C) 2019 All rights reserved.
'''

import math


class Solution(object):
    def checkPerfectNumber(self, num):
        """
        :type num: int
        :rtype: bool
        """
        if num < 0 or num%2 != 0:
            return False

        sqrt = int(math.sqrt(num))
        tmpsum = 1
        for i in range(2, sqrt+1):
            if num % i == 0:
                tmpsum = tmpsum + i + num/i

        if sqrt * sqrt == num:
            tmpsum -= sqrt

        return tmpsum == num
