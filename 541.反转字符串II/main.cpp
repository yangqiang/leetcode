/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年03月03日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <string>
using namespace std;

class Solution {
public:
    string reverseStr(string s, int k) {
        int size = s.size();
        int start = 0;
        for(start = 0; start + k-1 < size; start += 2*k){
            reverse(s, start, start+k-1);
        }

        if(start >= size){
            return s;
        }

        reverse(s, start, size-1); //剩余少于 k 个字符，将剩余的所有全部反转
        return s;
    }

    void reverse(string &s, int start, int end){
        while(start < end){
            swap(s[start++], s[end--]);
        }
    }
};

class Solution {
public:
    string reverseStr(string s, int k) {
        int size = s.size();
        int start = 0, end = 0;
        for(start; start < size; start += 2*k){
            end = min(start + k - 1, size - 1);
            reverse(s, start, end);
        }

        return s;
    }

    void reverse(string &s, int start, int end){
        while(start < end){
            swap(s[start++], s[end--]);
        }
    }
};
