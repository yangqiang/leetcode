class Solution(object):
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: int
        """
        dic1 = dict()
        count = 0
        for i in s:
            value = dic1.get(s)
            if value:
                del dic1[s]
                count++;
            else:
                dic1[s] = 1

        return count/2+1 if len(dic1) > 0 else count/2

        
