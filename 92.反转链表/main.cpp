#include <iostream>

using namespace std;

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* reverseBetween(ListNode* head, int m, int n) {
        if( m == n ){
            return head
        }

        int step = n - m;
        ListNode newhead= ListNode(0);
        newhead.next = head;
        ListNode* pre = &newhead;
        while( --m )
            pre = pre->next;

        ListNode* move = NULL;
        LiatNOde* p = pre->next;
        while( step-- ){
            move = p->next;
            p->next = move->next;
            move->next = pre->next;
            pre->next = move->next;
        }
        
        return newhead.next;
    }
};
