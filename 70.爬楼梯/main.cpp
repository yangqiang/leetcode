/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年06月04日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;

class Solution {
public:
    int climbStairs(int n) {
        if( n == 0 || n ==1 || n == 2 )
            return n;
    
        int *step = new int[n];
        step[0] = 1;
        step[1] = 2;
        for( int i = 2; i < n; i++ )
            step[i] = step[i -2] + step[i - 1];

        int tmp = step[n-1];
        delete[] step;
        return tmp; 
    }
};
