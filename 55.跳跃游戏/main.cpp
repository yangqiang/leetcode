/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年02月20日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;

class Solution {
public:
    bool canJump(vector<int>& nums) {   
        int len = nums.size();
        int maxreach = 0;
        int i = 0;
        for( i = 0; i < len && i <= maxreach; i++ ){
            maxreach = max(maxreach, nums[i] + i);
        }

        return i == len;
    }
};

