#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年03月31日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''


class Solution(object):
    def subdomainVisits(self, cpdomains):
        """
        :type cpdomains: List[str]
        :rtype: List[str]
        """

        dic = dict()
        for case in cpdomains:
            time, domain = case.split(' ')
            count = len(domain.split('.'))
            for num in range(count):
                dm = domain.split('.', num)[-1]
                val = dic.get(dm, 0)
                dic[dm] = int(time) + val

        res = list()
        for key, val in dic.iteritems():
            res.append(str(val)+" "+key)

        return res


def subdomainVisits(self, cpdomains):
    c = collections.Counter()
    for cd in cpdomains:
        n, d = cd.split()
        c[d] += int(n)
        for i in range(len(d)):
            if d[i] == '.': c[d[i + 1:]] += int(n)
    return ["%d %s" % (c[k], k) for k in c]