/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年02月15日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;

class Solution {
public:
    bool isUgly(int num) {
        if( num == 0 )
            return false;

        while( num%2 == 0 )
            num = num/2;
        
        while( num%3 == 0 )
            num = num/3;

        while( num%5 == 0 )
            num = num/5;

        return num == 1;
    }
};
