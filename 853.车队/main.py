#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年03月21日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''


class Solution(object):
    def carFleet(self, target, position, speed):
        """
        :type target: int
        :type position: List[int]
        :type speed: List[int]
        :rtype: int
        """

        pos = dict()
        if len(position) == 0:
            return 0

        for i in range(len(position)):
            pos[position[i]] = i

        position.sort(reverse=True)
        time = [0.0 for _ in range(len(position))]
        for i in range(len(position)):
            j = pos[position[i]]
            time[i] = float((target - position[i]))/speed[j]

        ans = 1
        last_max_time = time[0]

        for i in range(1, len(position)):
            if time[i] <= last_max_time:
                continue

            ans += 1
            last_max_time = time[i]

        return ans
