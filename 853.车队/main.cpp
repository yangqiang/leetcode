/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年03月21日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
using namespace std;

bool cmp(int &i, int &j){
    return i > j;
}

class Solution {
public:

    int carFleet(int target, vector<int>& position, vector<int>& speed) {
        int size = position.size();
        if(size == 0){
            return 0;
        }

        vector<float> time(size, 0.0);
        map<int, int> pos;
        for(int i = 0; i < size; i++){
            pos[position[i]] = i;
        }

        sort(position.begin(), position.end(), cmp);
        int k = 0;
        for(int i = 0; i < size; i++){
            k = pos[position[i]];
            time[i] = float((target - position[i]))/speed[k];
        }

        float last_max_time = time[0];
        int ans = 1;

        for(int i = 1; i < size; i++){
            if(time[i] <= last_max_time){
                continue;
            }

            ans++;
            last_max_time = time[i];
        }

        return ans;
    }
};