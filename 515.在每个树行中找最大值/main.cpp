/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年09月15日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <deque>
#include <vector>
#include <limits.h>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    deque<TreeNode *> q;
    vector<int> res;

public:
    vector<int> largestValues(TreeNode* root) {
        if(!root)
            return res;

        int len = 0, linemax = INT_MIN;
        TreeNode *p = NULL;
        q.push_back(root);
        while(!q.empty()){
            len = q.size();
            while(len > 0){
                p = q.front();
                q.pop_front();
                linemax = max(linemax, p->val);

                if(p->left){
                    q.push_back(p->left);
                }
                if(p->right){
                    q.push_back(p->right);
                }

                len--;
            }

            res.push_back(linemax);
            linemax = INT_MIN;
        }

        return res;
    }
};