/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年01月23日
*   描    述：
*   
================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
        int len = nums.size();
        vector<int> res(len);
        res[0] = 1;

        for( int i = 1; i < len; i++ ){
            res[i] = res[i-1] * nums[i-1];
        }

        int right = 1;
        for( int i = len - 1; i >= 0; i-- ){
            res[i] = res[i] * right;
            right = nums[i] * right;
        }

        return res;
    }
};

