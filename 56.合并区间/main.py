#!/usr/bin/env python3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年02月20日
描    述：
Copyright (C) 2019 All rights reserved.
'''

# Definition for an interval.
# class Interval(object):
#     def __init__(self, s=0, e=0):
#         self.start = s
#         self.end = e

# Definition for an interval.
# class Interval(object):
#     def __init__(self, s=0, e=0):
#         self.start = s
#         self.end = e

class Solution(object):
    def merge(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: List[Interval]
        """

        intervals.sort(key=lambda x:x.start)
        tmp = Interval()
        length = len(intervals)
        res = []
        i = 0
        while i < length:
            start = intervals[i].start
            end = intervals[i].end
            j = i + 1
            while j < length:
                tmp = intervals[j]
                if tmp.start <= end:
                    end = max(end, tmp.end)
                    j = j + 1
                else:
                    break
            tmpinterval = Interval(start, end)
            res.append(tmpinterval)
            i = j
        
        return res
