/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年12月15日
*   描    述：
*   
================================================================*/


#include <iostream>

using namespace std;

class Solution {
public:
    int hammingWeight(uint32_t n) {
        int result = 0;
        while( n != 0 ){
            result++;
            n = n & (n - 1);
        }

        return result;
    }
};
