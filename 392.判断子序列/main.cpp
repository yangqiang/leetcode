/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年04月28日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;

class Solution {
public:
    bool isSubsequence(string s, string t) {
        int tsize = t.size();
        int ssize = s.size();
        if(ssize == 0)
            return true;
        
        int cnt = 0;
        for(int i = 0; i < tsize; i++){
            if(s[cnt] == t[i]){
                cnt++;
            }
            if(cnt == ssize){
                return true; 
            }
        }

        return false;
    }
};
