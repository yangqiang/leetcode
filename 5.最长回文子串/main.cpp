#include <iostream>
#include <string>

using namespace std;

class Solution {
public:
    int pos = 0;
    int maxlen = 1;

    string longestPalindrome(string s) {
        int len = s.size();
        if( len < 2 )
            return s;
        
        for( int i = 0; i < len - 1; i++ ){
            extendPalindrome(s, i, i);
            extendPalindrome(s, i, i + 1);
        }

        return s.substr(pos, maxlen);
    }

private:
    void extendPalindrome(string &s, int left, int right){
        int len = s.size();
        while( left >= 0 && right <= len - 1 && (s[left] == s[right]) ){
            left--;
            right++;
        }

        if( maxlen < right - left + 1 ){
            maxlen = right - left + 1;
            pos = left;
        }
    }
};