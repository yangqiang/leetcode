#!/usr/bin/env python3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2018年11月26日
描    述：
Copyright (C) 2018 All rights reserved.
'''
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
class Solution:
    def countNodes(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if root == None:
            return 0;

        left = root.left
        right = root.right
        
        hl = 0; hr = 0;
        while left:
            hl += 1;
            left = left.left
        while right:
            hr += 1
            right = right.right

        if hr == hl:
            return pow(2, hl + 1) - 1

        return 1 + self.countNodes(root.left) + self.countNodes(root.right)
