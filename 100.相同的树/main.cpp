/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool isSameTree(TreeNode* p, TreeNode* q) {
        if ( p == NULL && q == NULL ){
            return true;
        }

        if( (p == NULL && q != NULL) || (p != NULL && q == NULL) ){
            return false;
        }

        if( p->val == q->val ){
            return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
        }
        
        return false;
    }
};/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年03月03日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;



/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool isSameTree(TreeNode* p, TreeNode* q) {
        if ( p == NULL & q == NULL ){
            return true;
        }

        if( (p == NULL && q != NULL) || (p != NULL && q == NULL) ){
            return false;
        }

        if( isSameTree(p->left, q->left) == false ){
            return false;
        }

        return isSameTree(p->right, q->right);
    }
};