/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* removeElements(ListNode* head, int val) {
        ListNode** t = &head;
        while( *t ){
            if( (*t)->val == val ){
                *t = (*t)->next;
            }else{
                t = &((*t)->next);
            }
        }

        return head;
    }
};
