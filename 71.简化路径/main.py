#!/usr/bin/env python3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2018年11月04日
描    述：
'''

class Solution:
    def simplifyPath(self, path):
        """
        :type path: str
        :rtype: str
        """
        path = [p for p in path.split("/") if p != "." and p != ""];
        stack = []
        for p in path:
            if p == "..":
                if len(stack) != 0:
                    stack.pop();
            else:
                stack.append(p)

        return "/" + "/".join(stack)
        
