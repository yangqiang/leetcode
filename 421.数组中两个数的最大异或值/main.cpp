#include <iostream>
#include <vector>
#include <set>

using namespace std;

class Solution {
public:
    int findMaximumXOR(vector<int>& nums) {
        int size = nums.size();

        int maxres = 0, flag = 0;
        int tmp = 0, num = 0;
        set<int> hashset;
        for(int bit = 31; bit >= 0; bit--){
            flag |= (1 << bit);
            for(int j = 0; j < size; j++){
                num = nums[j];
                hashset.insert(flag & num);
            }

            tmp = maxres | (1 << bit);
            for(set<int>::iterator it=hashset.begin() ;it!=hashset.end();it++){
                if(hashset.find(tmp ^ *it) != hashset.end()){
                    maxres = tmp;
                    break;
                }
            }

            hashset.clear();
        }

        return maxres;
    }
};
