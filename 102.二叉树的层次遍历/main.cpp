/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年11月05日
*   描    述：
*   Copyright (C) 2018 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <vector>
#include <stack>
#include <deque>
using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        vector<vector<int>> res;
        if( !root )
            return res;

        deque<TreeNode *> q;
        q.push_back(root);
        int size = 0;
        TreeNode *p = NULL;
        while(!q.empty()){
            size = q.size();
            vector<int> tmp;
            while(size--){
                p = q.front();
                tmp.push_back(p->val);

                q.pop_front();
            
                if(p->left)
                    q.push_back(p->left);
                if(p->right)
                    q.push_back(p->right);
            }

            res.push_back(tmp);

        }

        return res;
    }
};