#!/usr/bin/env python3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2018年11月25日
描    述：
'''
class Solution:
    def minSubArrayLen(self, s, nums):
        """
        :type s: int
        :type nums: List[int]
        :rtype: int
        """
        
        length = len(nums)
        if length == 0:
            return 0

        tmpsum = 0; minlen = length + 1
        begin = 0; end = 0;
        for i in range(length):
            tmpsum += nums[i];
            if tmpsum >= s:
                if begin == end:
                    return 1
                while tmpsum >= s and begin <= end:
                    minlen = min(minlen, end - begin + 1)
                    tmpsum -= nums[begin]
                    begin += 1
            end += 1

        return 0 if minlen == length + 1 else minlen
