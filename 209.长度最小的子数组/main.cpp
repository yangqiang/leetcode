/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年11月25日
*   描    述：
*   
================================================================*/


#include <iostream>
#include <vector>
using namespace std;
#if 0
class Solution {
public:
    int minSubArrayLen(int s, vector<int>& nums) {
        int len = nums.size();
        int minlen = INT_MAX, tmplen = 0, sum = 0;
        for( int i = 0; i < len - 1; i++ ){
            sum = nums[i];
            if( sum >= s )
                return 1;

            tmplen = 1;
            for(int j = i + 1; j < len; j++){
                sum += nums[j];
                tmplen += 1;
                if(sum >= s){
                    minlen = min(minlen, tmplen);
                    break;
                }
                
            }
        }

        return minlen == INT_MAX ? 0:minlen;
    }
};
#endif
class Solution {
public:
    int minSubArrayLen(int s, vector<int>& nums) {
        int len = nums.size();
        if( len == 0 )
            return 0;
        
        int sum = 0, minlen = INT_MAX;
        int begin = 0, end = 0;
        for( int i = 0; i < len; i++){
            sum += nums[i];
            if( sum >= s ) {
                if( begin == i )
                    return 1;
                while( sum >= s && begin <= i ){
                    minlen = min(minlen, i - begin + 1);
                    sum -= nums[begin++];
                }
            }
        }

        return minlen == INT_MAX ? 0 : minlen;
    }
};

