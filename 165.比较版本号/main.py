#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年06月15日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''


class Solution(object):
    def compareVersion(self, version1, version2):
        """
        :type version1: str
        :type version2: str
        :rtype: int
        """
        v1 = version1.split('.')
        v2 = version2.split('.')

        l1, l2 = len(v1), len(v2)

        for i in range(max(l1, l2)):
            i1 = int(v1[i]) if i < l1 else 0
            i2 = int(v2[i]) if i < l2 else 0
            if i1 != i2:
                return 1 if i1 > i2 else -1

        return 0

