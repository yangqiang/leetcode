/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年06月16日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
#include <string>
#include <stdio.h>
#include <string.h>
using namespace std;

class Solution {
public:
    int compareVersion(string version1, string version2) {
        int size1 = version1.size();
        int size2 = version2.size();

        vector<int> v1;
        vector<int> v2;

        char *tmp = new char[max(size1, size2)+1];
        int index = 0;
        int num = 0;
        memset(tmp, '\0', max(size1,size2));
        for(int i = 0; i < size1; i++){
            if(version1[i] == '.'){
                num = atoi(tmp);
                v1.push_back(num);
                memset(tmp, '\0', max(size1,size2)+1);
                index = 0;
            }else{
                tmp[index++] = version1[i];
            }
        }
        num = atoi(tmp);
        v1.push_back(num);

        index = 0;
        num = 0;
        memset(tmp, '\0', max(size1,size2));
        for(int i = 0; i < size2; i++){
            if(version2[i] == '.'){
                int num = atoi(tmp);
                v2.push_back(num);
                memset(tmp, '\0', max(size1,size2)+1);
                index = 0;
            }else{
                tmp[index++] = version2[i];
            }
        }
        num = atoi(tmp);
        v2.push_back(num);

        int size = max(v1.size(), v2.size());
        int i1 = 0, i2 = 0;
        int ret = 0;
        for(int i = 0; i < size; i++){
            i1 = i < v1.size() ? v1[i] : 0;
            i2 = i < v2.size() ? v2[i] : 0;

            if(i1 != i2){
                ret = i1 > i2 ? 1 : -1;
                break;
            }
        }
    
        delete []tmp;
        return ret;
    }
};

int main(){
    string version1 = "1.0";
    string version2 = "1";
    class Solution s;
    cout << s.compareVersion(version1, version2) << endl;
    return 0;
}