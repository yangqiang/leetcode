/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年07月22日
*   描    述：
*   
================================================================*/


#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
    void reverseString(vector<char>& s) {
        int size = s.size();
        if(size == 0){
            return;
        }

        char tmp;
        for(int i = 0, j = size - 1; j > i; i++, j--){
            tmp = s[i];
            s[i] = s[j];
            s[j] = tmp;
        }

        return;
    }
};
