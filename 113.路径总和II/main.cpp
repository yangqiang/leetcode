/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年12月23日
*   描    述：
*   Copyright (C) 2018 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <vector>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> pathSum(TreeNode* root, int sum) {
        vector<vector<int>> paths;
        vector<int> path;
        if( !root )
            return paths;

        findPath(root, sum, paths, path);

        return paths;
    }

private:
    void findPath(TreeNode* root, int sum, vector<vector<int>> &paths, vector<int> &path){
        path.push_back(root->val);
        
        if( !root->left && !root->right ){
            if( root->val == sum ){
                paths.push_back(path);
            }
            
            path.pop_back();
            return;
        }

        if( root->left )
            findPath(root->left, sum - root->val, paths, path);
        if( root->right )
            findPath(root->right, sum - root->val, paths, path);
        
        path.pop_back();
    }
};
