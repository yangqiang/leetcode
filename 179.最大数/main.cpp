/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年05月08日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <algorithm>
#include <string>
using namespace std;

class Solution {
public:
    static bool cmp(string a, string b){
        return a+b > b+a;
    }
    
    string largestNumber(vector<int>& nums) {
        int size = nums.size();
        if(size == 0)
            return "";

        vector<string> strs(size);
        for(int i = 0; i < size; i++){
            strs[i] = to_string(nums[i]);
        }

        sort(strs.begin(), strs.end(), cmp);
        string laststr = "";
        for(int i = 0; i < size; i++){
            laststr += strs[i];
        }

        if( laststr[0] == '0' )
            return "0";

        return laststr;

    }
};
