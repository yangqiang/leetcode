/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年12月28日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int findLength(vector<int>& A, vector<int>& B) {
        int sizeA = A.size(), sizeB = B.size();
        vector<vector<int>> data(sizeB);
        int res = 0;
        for(int i = 0; i < sizeB; i++){
            data[B[i]].push_back(i);
        }

        int tmp = -1, size = 0, tmpres = 0;
        for(int i = 0; i < sizeA; i++){
            tmp = A[i];
            size = data[tmp].size();

            if( size > 0){
                for(int j = 0; j < size; j++){
                    tmpres = 0;
                    int k = i, m = data[tmp][j];
                    while(k < sizeA && m < sizeB){
                        if(A[k] == B[m]){
                            tmpres++;
                        }else{
                            break;
                        }
                    }

                    res = max(res, tmpres);
                }
            }
        }

        return res;
    }
};

class Solution {
public:
    int findLength(vector<int>& A, vector<int>& B) {
        int sizeA = A.size();
        int sizeB = B.size();
        int res = 0;
        vector<vector<int>> dp(sizeA+1, vector<int>(sizeB+1, 0));
        for(int i = sizeA-1; i >= 0; i--){
            for(int j = sizeB-1; j >= 0; j--){
                if(A[i] == B[j]){
                    dp[i][j] = dp[i+1][j+1] + 1;
                    res = max(res, dp[i][j]);
                }
            }
        }

        return res;
    }
};