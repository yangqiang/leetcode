#!/usr/bin/env python3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年02月18日
描    述：
Copyright (C) 2019 All rights reserved.
'''

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None


class Solution:
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        
        L1 = l1
        L2 = l2
        res = ListNode(0)
        tmp = res
        while L1 and L2:
            tmp.next = ListNode(L1.val + L2.val)
            tmp = tmp.next
            L1 = L1.next
            L2 = L2.next

        tmp.next = L1 if L1 else L2
        tmp = res.next
        while tmp:
            if tmp.val >= 10:
                if not tmp.next:
                    tmp.next = ListNode(0)
                tmp.val = tmp.val - 10
                tmp.next.val = tmp.next.val + 1
            tmp = tmp.next

        return res.next


class Solution:
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """

        L1 = l1
        L2 = l2
        while L1 and L2:
            L1.val = L2.val = L1.val + L2.val
            L1 = L1.next
            L2 = L2.next

        res = l1 if L1 else l2
        tmp = res
        while tmp:
            if tmp.val >= 10:
                if not tmp.next:
                    tmp.next = ListNode(0)
                tmp.val = tmp.val - 10
                tmp.next.val = tmp.next.val + 1
            
            tmp = tmp.next
            
        return res
