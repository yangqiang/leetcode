/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年03月20日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    vector<vector<int>> largeGroupPositions(string S) {
        vector<vector<int>> res;
        int size = S.size();
        if(size <= 2){
            return res;
        }

        vector<int> tmp;
        int start = 0, end = 0 ;
        for(int i = 1; i < size; i++){
            if(S[i] != S[i-1]){
                if(i - start -1 >= 2){
                    tmp.clear();
                    tmp.push_back(start);
                    tmp.push_back(i-1);
                    res.push_back(tmp);
                }

                start = i;
            }
        }

        if(S[size - 1] == S[size - 2]){
            if(size - 1 - start >= 2){
                tmp.clear();
                tmp.push_back(start);
                tmp.push_back(size-1);
                res.push_back(tmp);
            }
        }
        
        return res;
    }
};