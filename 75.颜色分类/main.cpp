/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年05月18日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    void sortColors(vector<int>& nums) {
       int low = 0, cur = 0;
       int hi = nums.size() - 1;
       while(cur <= hi){
           if(nums[cur] == 0){
               nums[cur++] = nums[low];
               nums[low++] = 0;
           }else if(nums[cur] == 2){
                nums[cur] = nums[hi];
                nums[hi--] = 2;
           }else{
               cur++;
           }
       }
    }
};