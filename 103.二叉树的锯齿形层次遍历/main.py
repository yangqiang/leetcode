#!/usr/bin/env python3
#-*- coding:utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')
'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年03月05日
描    述：
'''

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def zigzagLevelOrder(self, root: TreeNode) -> List[List[int]]:
        if root == None:
            return []

        res = []
        leftflag = True

        queue = []
        queue.append(root)
        while len(queue):
            length = len(queue)
            tmp = []
            while length > 0:
		if leftflag == True:
                    p = queue.pop(0)
                else:
                    p = queue.pop()

                tmp.append(p.val)
                if p.left:
                    queue.append(p.left)
                if p.right:
                    queue.append(p.right)
                length = length - 1
            if leftflag == True:
                res.append(tmp)
                leftflag = False
            else:
                res.append(tmp)
                leftflag = True

        return res
