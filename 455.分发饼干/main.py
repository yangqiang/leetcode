class Solution(object):
    def findContentChildren(self, g, s):
        """
        :type g: List[int]
        :type s: List[int]
        :rtype: int
        """
        g.sort()
        s.sort()

        child_size = len(g)
        cookie_size = len(s)
        i = 0
        j = 0
        while i < child_size and j < cookie_size:
            if g[i] <= s[j]:
                i += 1
            j += 1

        return i
