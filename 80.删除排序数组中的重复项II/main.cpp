/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年05月24日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        int size = nums.size();
        if(size <= 1){
            return size;
        }

        int j = 0, i = 0;
        int pos = 0;
        for(i = 0; i < size; i++){
            if(nums[i] == nums[j]){
                while(i < size && nums[i] == nums[j]){
                    i += 1;
                }
                
                i -= 1;
            }
            
            nums[pos++] = nums[i];
            if(j != i){
                nums[pos++] = nums[i];

            }

            j = i + 1;
        }

        return pos;
    }
};