/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年07月25日
*   描    述：
*   
================================================================*/


#include <iostream>

using namespace std;

class Solution {
public:
    vector<int> singleNumber(vector<int>& nums) {
        int len = nums.size();
        vector<int> result(2, 0);
        if( len == 2 ){
            result[0] = nums[0];
            result[1] = nums[1];
            return result;
        }
        
        int num = 0;
        for( int i = 0; i < len; i++ ){
            num ^= nums[i];
        }

        int flag = num & -num;
        for( int j = 0; j < len; j++ ){
            if( nums[j] & flag )
                result[0] ^= nums[j];
            else 
                result[1] ^= nums[j];
        }
        
        return result;
    }
};
