/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年03月07日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int findLengthOfLCIS(vector<int>& nums) {
	    int size = nums.size();
        int res = 0;
        int pos = 0;
        for(int i = 1; i < size; i++){
            if(nums[i-1] >= nums[i]){
                res = max(res, i - pos);
                pos = i;
            }
        }

        return max(res, size - pos);
    }
};