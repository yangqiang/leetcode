class Solution(object):
    def thirdMax(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        data = list()
        for num in nums:
            if len(data) == 3:
                if num == data[0] or num == data[1] or num == data[2]:
                    continue
                if num > data[2]:
                    data[0] = data[1]
                    data[1] = data[2]
                    data[2] = num
                elif num > data[1]:
                    data[0] = data[1]
                    data[1] = num
                elif num > data[0]:
                    data[0] = num
            elif len(data) == 2:
                if num == data[0] or num == data[1]:
                    continue
                if num > data[1]:
                    data.append(num)
                    continue
                elif num > data[0]:
                    data.append(data[1])
                    data[1] = num
                else:
                    data.append(data[1])
                    data[1] = data[0]
                    data[0] = num
            elif len(data) == 1:
                if num == data[0]:
                    continue
                if num > data[0]:
                    data.append(num)
                else:
                    data.append(data[0])
                    data[0] = num
            else:
                data.append(num)

        return data[len(data)-1] if len(data) < 3 else data[0]
