/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年05月06日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* pruneTree(TreeNode* root) {
        return containsOne(root) == true? root : NULL;
    }

    bool containsOne(TreeNode* root){
        if(root == NULL){
            return false;
        }

        bool left = containsOne(root->left);
        bool right = containsOne(root->right);

        if(!left){
            root->left = NULL;
        }
        if(!right){
            root->right = NULL;
        }

        return root->val == 1 ? true : left|right;
    }
};