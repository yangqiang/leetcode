/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年07月15日
*   描    述：
*   Copyright (C) 2018 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <vector>
#include <map>


using namespace std;

class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        vector<int> result;
        int len = nums.size();
        if( len == 0 || len == 1 )
            return result;

        map<int, int> hsmap;
        for( int i = 0; i < len; i++ ){
            hsmap.insert(pair<int, int> (nums[i], i));
        }

        int tmp = 0, pos = 0;
        for(int j = 0; j < len; j++){
            tmp = target - nums[j];
            if( hsmap.count(tmp) ){
                pos = hsmap[(tmp)];
                if( j != pos ){
                    result.push_back(j);
                    result.push_back(pos);
                    break;
                }
            }
        }

        return result;
    }
};
