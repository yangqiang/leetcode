/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年01月09日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    vector<int> findDisappearedNumbers(vector<int>& nums) {
        int size = nums.size();
        vector<int> res;

        nums.push_back(size);
        for(int i = 0; i < size; i++){
            if(nums[i] <= 0){
                if(nums[nums[i] + size] <= 0){
                    continue;
                }

                nums[nums[i] + size] -= size;
            }else{
                if( nums[nums[i]] > 0 ){
                    nums[nums[i]] -= size;
                }
            }
        }

        for(int i = 1; i <= size; i++){
            if(nums[i] > 0){
                res.push_back(i);
            }
        }

        return res;
    }
};