/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年12月15日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution {
public:
    int maximumSwap(int num) {
        string s;
        int tmp = 0;
        while(num > 0){
            tmp = num%10;
            s.push_back(tmp + '0');
            num = num/10;
        }

        vector<int> data(10, -1);
        int size = s.size(), pos = -1;
        for(int i = size - 1; i >= 0; i--){
            data[s[i] - '0'] = i;
        }

        for(int i = size - 1; i >= 0; i--){
            tmp = s[i] - '0';
            for(int j = 9; j > tmp; j--){
                if(data[j] >= 0 && data[j] < data[tmp]){
                    pos = data[j];
                    break;
                }
            }
            if(pos >= 0){
                char c = s[i];
                s[i] = s[pos];
                s[pos] = c;
                break;
            }
        }

        for(int i = size - 1; i >= 0; i--){
            tmp = s[i] - '0';
            num = num*10 + tmp;           
        }

        return num;
        
    }
};class Solution {
public:
    int maximumSwap(int num) {
        string s;
        int tmp = 0;
        while(num > 0){
            tmp = num%10;
            s.push_back(tmp);
            num = num/10;
        }

        vector<int> data(10, -1);
        int size = s.size(), pos = -1;
        for(int i = size - 1; i >= 0; i--){
            data[s[i]] = i;
        }

        for(int i = size - 1; i >= 0; i--){
            tmp = s[i];
            for(int j = 9; j > tmp; j--){
                if(data[j] >= 0 && data[j] < i){
                    pos = data[j];
                    break;
                }
            }
            if(pos >= 0){
                char c = s[i];
                s[i] = s[pos];
                s[pos] = c;
                break;
            }
        }

        for(int i = size - 1; i >= 0; i--){
            tmp = s[i];
            num = num*10 + tmp;           
        }

        return num;
        
    }
};